from qutipy.general_functions import *

import numpy as np 
import scipy.linalg
import math
#import import_ipynb 
import random
#from joblib  import Parallel, delayed 
import matplotlib.pyplot as plt
import time
from scipy.linalg import logm

mu = 1; N = 12; Jsq = 1; hc=1; dims = np.array([2]*N);

P_l = np.array([[0,0],[1,0]]);
P_r = np.array([[0,1],[0,0]]);
P_z = np.array([[1,0],[0,-1]]);
p_up = np.array([[1,0],[0,0]]); p_dw = np.array([[0,0],[0,1]]);
C = [];

# loading data
import hickle as hkl
data = hkl.load('ham12.hkl')
H = data['ham']

p_up = np.asarray([[1,0], [0,0]])
p_down = np.asarray([[0,0], [0,1]])
#P = [p_up, p_down]

p_up_array = []
for i in range(N):
    if i == 0:
        temp = p_up
    else:
        temp = np.eye(2)
    for j in range(1,N):
        if j == i:
            temp = np.kron(temp, p_up)
        else:
            temp = np.kron(temp, np.eye(2))
    p_up_array.append(temp)
    
down_up = [np.asarray([[0],[1]]), np.asarray([[1],[0]])]

from qutip import basis, tensor, Qobj, mesolve

"""psi_list = []
psi_list.append(basis(2,1))
for n in range(N-1):
    psi_list.append((1/np.sqrt(2))*(basis(2,0)+basis(2,1)))
psi0 = tensor(psi_list)"""

"""psi_list = []
psi_list.append(basis(2,0))
psi_list.append(basis(2,1))
for n in range(N-2):
    psi_list.append(basis(2,0))
psi0 = tensor(psi_list)"""



tlist = np.linspace(0, 30, 300)

qH = Qobj(H)
temp = np.array([2]*N)
qH.dims = [temp, temp]

statesstates = []

p = 0.2 #measure all particles with probability p

for k in range(200):
    print("Run number: ", k)
    states = []

    psi = np.zeros(2**N)
    psi[1] = 1
    psi0 = Qobj(psi, dims = [[2]*N,[1]*N ])

    psi = psi0
    for i in range(1,len(tlist)):
        #print(i)
        times = [tlist[i-1], tlist[i]]
        #evolve for one timestep
        psi = mesolve(qH, psi, times).states[1].data.toarray()
        states.append(psi)
        rho = psi @ dag(psi)
        #perform probabilistic projective measurement
        if np.random.rand() <= p:
            measure_array = [1]*N #zero means project up, one means project down
            #rho_prob = np.zeros(N)
            for j in range(N):
                j_up = np.trace(p_up_array[j]*rho*p_up_array[j])
                if np.random.rand() <= j_up: #store in measure_array
                    measure_array[j] = 0
            #now we create the appropriate product pure state
            #psi = down_up[measure_array[0]]
            psi = basis(2, measure_array[0])
            for j in range(1,N):
                #psi = np.kron(psi, down_up[measure_array[j]])
                psi = tensor(psi, basis(2, measure_array[j]))
        #psi = Qobj(psi, dims = [np.array([2]*N), np.array([1]*N)] )
        else:
            psi = Qobj(psi, dims = [[2]*N, [1]*N])

    statesstates.append(states)




    entropiesentropies = []

    entropies = []
    for i in range(len(tlist)-1):
        #print(i)
        psi = states[i]
        #psi = psi.toarray()
        rho = psi @ dag(psi)
        rho = partial_trace(rho, [1,2,3,4,5,6], [2,2,2,2,2,2,2,2,2,2,2,2])
        entropy = -np.real(Tr(rho@logm(rho)))/np.log(2)
        entropies.append(entropy)

    entropiesentropies.append(entropies)

data = {'statesstates': statesstates, 'entropiesentropies': entropiesentropies}
hkl.dump( data, 'data12.hkl')