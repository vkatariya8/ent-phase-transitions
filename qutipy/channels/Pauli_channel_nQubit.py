'''
This code is part of QuTIpy.

(c) Copyright Sumeet Khatri, 2021

This code is licensed under the Apache License, Version 2.0. You may
obtain a copy of this license in the LICENSE.txt file in the root directory
of this source tree or at http://www.apache.org/licenses/LICENSE-2.0.

Any modifications or derivative works of this code must retain this
copyright notice, and modified files need to carry a notice indicating
that they have been altered from the originals.
'''


import numpy as np
import itertools


from qutipy.Pauli import generate_nQubit_Pauli,generate_nQubit_Pauli_X, generate_nQubit_Pauli_Z
from qutipy.channels import generate_channel_isometry


def Pauli_channel_nQubit(n,p,alt_repr=False):

    '''
    Generates the Kraus operators, an isometric extension, and a unitary
    extension of the n-qubit Pauli channel specified by the 2^(2*n) parameters in
    p, which must be probabilities in order for the map to be a channel. (i.e.,
    they must be non-negative and sum to one.)

    If alt_repr=True, then the channel is of the form

    P(rho)=\sum_{a,b} p_{a,b} X^aZ^b(rho)Z^bX^a

    where a and b are n-bit strings
    (using the n-qubit X and Z operators as generated by the functions
    generate_nQubit_Pauli_X and generate_nQubit_Pauli_Z).
    '''

    K=[]

    if not alt_repr:
        S=list(itertools.product(*[range(0,4)]*n))
        for i in range(2**(2*n)):
            K.append(np.sqrt(p[i])*generate_nQubit_Pauli(list(S[i])))

        V,U=generate_channel_isometry(K,2**n,2**n)

        return K,V,U

    else:  #alt_repr==True
        S=list(itertools.product(*[range(0,2)]*n))
        count=0
        for a in S:
            a=list(a)
            for b in S:
                b=list(b)
                K.append(np.sqrt(p[count])*generate_nQubit_Pauli_X(a)@generate_nQubit_Pauli_Z(b))
                count+=1

        V,U=generate_channel_isometry(K,2**n,2**n)

        return K,V,U