from qutipy.general_functions import *
import numpy as np 
import scipy.linalg
import math
#import import_ipynb 
import random
from joblib  import Parallel, delayed 
import matplotlib.pyplot as plt
import time

mu = 1; N = 14; Jsq = 1; hc=1; dims = np.array([2]*N);

P_l = np.array([[0.,0.],[1.,0.]]);
P_r = np.array([[0.,1.],[0.,0.]]);
P_z = np.array([[1.,0.],[0.,-1.]]);
p_up = np.array([[1.,0.],[0.,0.]]); p_dw = np.array([[0.,0.],[0.,1.]]);
C = [];

def ham_outer_loop(i, C, N, mu=1):
    print("Outer loop: ", i)
    H = np.zeros([2**N, 2**N])
    for j in range(N):
        temp = np.transpose(C[i]) @ np.transpose(C[j])
        for k in range(N):
            temp = temp @ C[k]
            for l in range(N):
                H = H + J[i,j,k,l]*(temp @ C[l])
    H *= (2*N)**(-1.5)
    H = H - mu*(np.transpose(C[i]) @ C[i]);
    return H

print("Making Ci's")
C = []
ci = P_l;
print("0")
for q in range(1,N):
    ci = np.kron(ci,np.identity(2)); 
C.append(ci);

for i in range(1,N): #Jordan Wigner transform
    print(i)
    ci = P_z;
    for q in range(1,i):
        ci = np.kron(ci,P_z);
    ci = np.kron(ci,P_l);
    for q in range(i,N-1):
        ci = np.kron(ci,np.identity(2)); 
    C.append(ci);
    
# making a random J
print("Making J")
J_temp = np.random.normal(0,Jsq,[N,N,N,N]);
J_sym = 0.5*(J_temp + J_temp.transpose([2,3,0,1]));
J_1 = 0.5*(J_sym - J_sym.transpose([1,0,2,3])); J = 0.5*(J_1 - J_1.transpose([0,1,3,2]));


"""H = np.zeros([2**N, 2**N]);
print("Making H")
for i in range(N): #instantiating Hamiltonian 
    print("Outer loop counter:", i)
    for j in range(N):
        print("Inner loop counter:", j)
        temp = np.transpose(C[i]) @ np.transpose(C[j])
        for k in range(N):
            temp = temp @C[j]
            for l in range(N):
                H = H + (2*N)**(-1.5)*J[i,j,k,l]*(temp @ C[l])
    H = H - mu*(np.transpose(C[i]) @ C[i]);"""

start = time.perf_counter()

print("Starting to make Hamiltonian")

H = Parallel(n_jobs=N)(delayed(ham_outer_loop)(i,C,N,mu) for i in range(N))
H = np.sum(H, 0)

end = time.perf_counter()
execution_time = (end - start)
print(execution_time)

import h5py
h5f = h5py.File('ham_14.h5', 'w')
h5f.create_dataset('ham_14', data=H)
h5f.close()




# Dump data to file

import hickle as hkl

data = {'ham': H}
hkl.dump( data, 'ham14_second.hkl' )