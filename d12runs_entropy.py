from qutipy.general_functions import *

import numpy as np 
import scipy.linalg
import math
#import import_ipynb 
import random
#from joblib  import Parallel, delayed 
import matplotlib.pyplot as plt
import time

mu = 1; N = 12; Jsq = 1; hc=1; dims = np.array([2]*N);

import hickle as hkl
data = hkl.load('data12.hkl')
#e = data['entropiesentropies']

statesstates = data['statesstates']

entropies = np.zeros(300)
from scipy.linalg import logm
for k in range(200):
    print(k)
    states = statesstates[k]
    for j in range(299):
        psi = states[j]
        rho = psi @ dag(psi)
        rho = partial_trace(rho, [1,2,3,4,5,6], [2]*12)
        entropy = -np.real(Tr(rho@logm(rho)))/np.log(2)
        entropies[j] += entropy
        
entropies /= 300

data = {'entropies': entropies}
hkl.dump( data, 'entropy12.hkl')